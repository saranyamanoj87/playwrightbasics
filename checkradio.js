const {chromium } = require('playwright');

( async() => {
    //function code
const browser = await chromium.launch({headless:false, slowMo: 300});
const page = await browser.newPage();
await page.goto('https://www.w3schools.com/howto/howto_css_custom_checkbox.asp', { 
  waitUntil: 'domcontentloaded', 
  timeout: 60000 // 60 seconds
});;
//check the secod checkbo
const checkbox = await page.$$('#main div :nth-child(1) input[type="checkbox"]');
await checkbox[1].check();
await checkbox[0].uncheck();
//select the second radio button
const radioBtn = await page.$$('#main div :nth-child(1) input[type="radio"]');
await radioBtn[1].check();
await browser.close();
})();