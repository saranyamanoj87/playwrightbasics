const {chromium } = require('playwright');

( async() => {
    //function code
const browser = await chromium.launch({headless:false, slowMo: 100});
const page = await browser.newPage();
await page.goto('https://www.thebay.com/account/login');
//code to type in email textbox
const email = await page.$('#login-form-email');
await email.type('expressqa000@mailinator.com',{ delay : 50});
await browser.close();
})();